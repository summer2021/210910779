# Navigation Safety Node (Id- 210910779)

## Community: **ROS**

### Project Description:
This project aims to create a safety watchdog node to ensure the robot
is acting properly and not about to collide with an obstacle. Typical
safety-rated lidars will contain “safety zones” whereas if any sensor points are
located in a box around the lidar, then the lidar will send a signal to the robot
to stop due to a potential collision. However, fewer and fewer people are using
safety-rated lidars as consumer-available lidars are dropping in cost and 3D
lidars are seeing more use in mobile robotics.

